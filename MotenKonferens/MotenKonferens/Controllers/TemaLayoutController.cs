﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;


namespace MK.Controllers
{
    public class TemaLayoutController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/SiteLayout/";
        public ActionResult RenderImageArchive()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_ImageArchive.cshtml");
        }
    }
}